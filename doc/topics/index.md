

# 深入了解 Authing Android Guard 组件设计细节

一般来说，头部 App 厂商会维持一个 10 人左右的团队，全职开发和维护认证相关的界面和逻辑。这是一笔不小的开销，原因在于登录界面的易用性，健壮性直接决定了付费用户转换率。想象一下，多少次当一个 App 让你登录的时候，也就是你放弃这个 App 的时候。而为了获得付费用户，又必须首先让用户登录。

另外一方面，安全性也需要巨大的投入。安全问题的特殊性在于，如果不出问题感觉不需要投入，可一旦出问题，损失都非常惨重。

Authing 作为认证领域专家，将数年认证经验沉淀在 Guard 组件里面，可以极大节约开发成本，降低安全风险。接下来，我们深入探讨 Authing 在设计 Guard 组件时所面临的巨大挑战以及我们的解决方案，同时站在软件开发的角度聊一些技术细节。

<br>

## 开始之前

我们在 Guard 设计开发之前，花了大量时间对业界主流 App 登录界面进行了深入分析。

[查看报告](./../auth_reports.md)

<br>

## 一 设计理念
1. [身份](./identity.md)
2. [语义化编程思想 ](./design.md)

## 二 主流程
1. [手机号一键登录](./oneauth.md)
2. 其他快速注册的手段
3. 注册信息补全
4. 支持多种形式的用户名
5. 关于短信验证码的思考
6. 合并注册与登录
7. 登录按钮的设计以及回调
8. 根据后台配置自动触发 MFA（Multi-Factor Authentication）
9. MFA 详解以及手动触发
10. 登出

## 三 UX & UI 交互和界面
1. 提示语位置思考
2. Splash 界面和自动登录
3. 为什么不使用 TextInputLayout

## 四 更多认证方式
1. [社会化登录](./../social/social.md)
2. 扫描二维码登录
3. 通过 Biometric API 支持指纹登录
4. 人脸识别登录

## 五 高级功能
1. 使用 Authing 提供的网络拦截器发送业务请求
2. 将你的 App 作为认证源
3. 自带监控能力：Crash，Log，设备分布，用户画像
4. 实名认证
5. 通过 Authing 接入虚拟网络，实现在移动端访问公司内网
6. 防截屏
7. 给图片加上带个人信息的水印
8. 远程擦除本地数据
9. Authing Audit（审计功能）
10. 设备互斥与强制下线
11. 深色模式
12. 多设备适配
13. [AppAuth](./app_auth.md)